﻿
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "cublas_v2.h"

#include <iostream>
#include <fstream>
#include <limits>
#include <cmath>
#include <chrono>
#include <algorithm>
#include <random>

#define M_PI 3.14159265358979323846

#define WIDTH 1920
#define HEIGHT 1080

#define NTHREADS 512

#define NSPHERES 20
#define NLIGHTS 3

#define MAXDEPTH 3

typedef struct
{
    float x;
    float y;
    float z;
} Vec3f;

typedef struct
{
    Vec3f position;
    float intensity;
} Light;

typedef struct
{
    Vec3f diffuseColor;
    Vec3f albedo;
    float specularExponent;
} Material;

typedef struct
{
    Vec3f center;
    float radius;
    Material material;
} Sphere;

// Image in PPM format
typedef struct
{
    int width;
    int height;
    Vec3f* buff;
} Image;

// https://github.com/leimao/PPMIO/blob/master/libs/ppm_io.cpp
bool readSkyboxImage(const char* filename, Image& skyboxImage)
{
    std::ifstream ifs(filename, std::fstream::binary);

    if (!ifs.is_open())
    {
        std::cout << "Failed to open " << filename << std::endl;
        return false;
    }

    std::string mMagic;
    ifs >> mMagic;
    ifs.seekg(1, ifs.cur);
    char c;
    ifs.get(c);
    if (c == '#')
    {
        while (c != '\n') ifs.get(c);
    } else
    {
        ifs.seekg(-1, ifs.cur);
    }

    int imgWidth, imgHeight, maxVal;
    ifs >> imgWidth >> imgHeight >> maxVal;
    if (maxVal != 255)
    {
        std::cout << "Failed to read " << filename << std::endl;
        std::cout << "Got PPM maximum value: " << maxVal << std::endl;
        std::cout << "Maximum pixel has to be 255" << std::endl;
        return false;
    }

    unsigned char* buff = new unsigned char[imgHeight * imgWidth * 3];
    ifs.seekg(1, ifs.cur);
    ifs.read((char*)buff, imgHeight * imgWidth * 3);

    skyboxImage.width = imgWidth;
    skyboxImage.height = imgHeight;
    skyboxImage.buff = new Vec3f[imgHeight * imgWidth];

    for (int i = 0, j = 0; i < imgHeight * imgWidth; ++i, j += 3)
    {
        skyboxImage.buff[i].x = buff[j] / 255.0f;
        skyboxImage.buff[i].y = buff[j + 1] / 255.0f;
        skyboxImage.buff[i].z = buff[j + 2] / 255.0f;
    }

    delete[] buff;

    return true;
}

void saveFrameToFile(Vec3f* framebuff)
{
    // Save framebuff to file
    std::ofstream ofs;
    ofs.open("./out.ppm", std::ios::binary);
    ofs << "P6\n" << WIDTH << " " << HEIGHT << "\n255\n";
    for (size_t i = 0; i < HEIGHT * WIDTH; ++i)
    {
        Vec3f& c = framebuff[i];
        float max = fmaxf(c.x, fmaxf(c.y, c.z));
        if (max > 1)
        {
            c.x *= (1.f / max);
            c.y *= (1.f / max);
            c.z *= (1.f / max);
        }
        ofs << (char)(255 * std::max(0.f, std::min(1.f, framebuff[i].x)));
        ofs << (char)(255 * std::max(0.f, std::min(1.f, framebuff[i].y)));
        ofs << (char)(255 * std::max(0.f, std::min(1.f, framebuff[i].z)));
    }
    ofs.close();
}

__device__ Vec3f multiplyAlpha(const Vec3f& v, float a)
{
    Vec3f res;
    res.x = v.x * a;
    res.y = v.y * a;
    res.z = v.z * a;
    return res;
}

__device__ Vec3f add(const Vec3f& a, const Vec3f& b)
{
    Vec3f res;
    res.x = a.x + b.x;
    res.y = a.y + b.y;
    res.z = a.z + b.z;
    return res;
}

__device__ Vec3f substract(const Vec3f& a, const Vec3f& b)
{
    Vec3f res;
    res.x = a.x - b.x;
    res.y = a.y - b.y;
    res.z = a.z - b.z;
    return res;
}

__device__ float dot(const Vec3f& a, const Vec3f& b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

__device__ float length(const Vec3f& v)
{
    return sqrtf(v.x * v.x + v.y * v.y + v.z * v.z);
}

__device__ Vec3f normalize(const Vec3f& v)
{
    float l = length(v);
    return multiplyAlpha(v, 1.f/l);
}

__device__ Vec3f reflect(const Vec3f& I, const Vec3f& N)
{
    return substract(I, multiplyAlpha(multiplyAlpha(N, 2.f), dot(I, N)));
}

__device__ bool rayIntersect(const Sphere* sphere, const Vec3f& orig, const Vec3f& dir, float& t0)
{
    Vec3f L = substract(sphere->center, orig);
    float tca = dot(L, dir);
    float d2 = dot(L, L) - tca * tca;
    float r = sphere->radius;
    if (d2 > r * r) return false;
    float thc = sqrtf(r * r - d2);
    t0 = tca - thc;
    float t1 = tca + thc;
    if (t0 < 0) t0 = t1;
    if (t0 < 0) return false;
    return true;
}

__device__ bool sceneIntersect(const Vec3f& orig, const Vec3f& dir, const Sphere* spheres, Vec3f& hit, Vec3f& N, Material& material)
{
    float maxDist = 10000.f;

    for (int i = 0; i < NSPHERES; ++i)
    {
        float currDist;
        if (rayIntersect(&spheres[i], orig, dir, currDist) && currDist < maxDist)
        {
            maxDist = currDist;
            hit = add(orig, multiplyAlpha(dir, currDist));
            N = normalize(substract(hit, spheres[i].center));
            material = spheres[i].material;
        }
    }

    return maxDist < 10000.f;
}

__device__ Vec3f getSkyboxColor(const Vec3f& dir, const Image* skybox)
{
    float u = 0.5f + atan2f(dir.x, dir.z) / (2 * M_PI);
    float v = 0.5f - asinf(dir.y) / M_PI;

    int x = u * skybox->width;
    int y = v * skybox->height;

    return skybox->buff[y * skybox->width + x];
}

typedef struct
{
    Vec3f orig;
    Vec3f dir;
    Vec3f hitPoint;
    Vec3f N;
    Material material;
    Vec3f reflectColor;
} RaycastStackInfo;

__device__ Vec3f castRay(const Vec3f& orig, const Vec3f& dir, const Sphere* spheres, const Light* lights, const Image* skybox)
{
    Vec3f hitPoint, N;
    Material material; material.albedo = { 1,0,0 };
    int depth = 0;
    RaycastStackInfo stack[MAXDEPTH+1];

    Vec3f o = orig, d = dir;

    while (depth < MAXDEPTH)
    {
        stack[depth] = { o , d };
        if (!sceneIntersect(o, d, spheres, hitPoint, N, material))
        {
            stack[depth].reflectColor = getSkyboxColor(d, skybox);
            break;
        }
        else
        {
            stack[depth].hitPoint = hitPoint;
            stack[depth].N = N;
            stack[depth].material = material;
            Vec3f reflectDir = normalize(reflect(dir, N));
            Vec3f reflectOrig = dot(reflectDir, N) < 0 ? substract(hitPoint, multiplyAlpha(N, 1e-3)) : add(hitPoint, multiplyAlpha(N, 1e-3));
            d = reflectDir;
            o = reflectOrig;
            ++depth;
        }
    }

    stack[MAXDEPTH].reflectColor = { 0.f, 0.f, 0.f };

    for (int j = depth-1; j >= 0; --j)
    {
        RaycastStackInfo* rsi = &stack[j];
        Vec3f reflectColor = stack[j+1].reflectColor;
        Vec3f hitPoint = rsi->hitPoint;
        Vec3f N = rsi->N;
        Material material = rsi->material;

        float diffuseLightIntensity = 0.f;
        float specularLightIntensity = 0.f;
        for (int i = 0; i < NLIGHTS; ++i)
        {
            Vec3f lightDir = normalize(substract(lights[i].position, hitPoint));
            float lightDist = length(substract(lights[i].position, hitPoint));

            Vec3f shadowOrig = dot(lightDir, N) < 0 ? substract(hitPoint, multiplyAlpha(N, 1e-3)) : add(hitPoint, multiplyAlpha(N, 1e-3));
            Vec3f shadowPoint, shadowN;
            Material tmpMat;

            if (sceneIntersect(shadowOrig, lightDir, spheres, shadowPoint, shadowN, tmpMat))
            {
                if (length(substract(shadowPoint, shadowOrig)) < lightDist) continue;
            }

            diffuseLightIntensity += lights[i].intensity * fmaxf(0.f, dot(lightDir, N));
            specularLightIntensity += powf(fmaxf(0.f, dot(multiplyAlpha(reflect(multiplyAlpha(lightDir, -1.f), N), -1.f), dir)), material.specularExponent) * lights[i].intensity;
        }

        Vec3f ones = { 1.f, 1.f, 1.f };
        Vec3f diffuse = multiplyAlpha(material.diffuseColor, diffuseLightIntensity * material.albedo.x);
        Vec3f spec = multiplyAlpha(ones, specularLightIntensity * material.albedo.y);
        Vec3f reflection = multiplyAlpha(reflectColor, material.albedo.z);
        rsi->reflectColor = add(diffuse, add(spec, reflection));
    }

    return stack[0].reflectColor;
}

__global__ void renderPixelKernel(Vec3f* framebuff, Sphere* spheres, Light* lights, Image* skybox, Vec3f* skyboxBuff, const int _rpt)
{
    skybox->buff = skyboxBuff;

    int id = blockIdx.x * blockDim.x + threadIdx.x;
    int step = NTHREADS / _rpt * WIDTH * HEIGHT / NTHREADS;

    for (int rpt = 0; rpt < _rpt && id < WIDTH * HEIGHT; ++rpt, id += step)
    {
        int j = id / WIDTH;
        int i = id % WIDTH;

        float fov = M_PI / 1.75;

        float x = (2 * (i + 0.5f) / (float)WIDTH - 1) * std::tan(fov / 2.) * WIDTH / (float)HEIGHT;
        float y = -(2 * (j + 0.5) / (float)HEIGHT - 1) * std::tan(fov / 2.);

        Vec3f dir = { x, y, -1.f };
        dir = normalize(dir);
        Vec3f orig = { 0.f, 0.f, 0.f };

        framebuff[j * WIDTH + i] = castRay(orig, dir, spheres, lights, skybox);
    }
}

int main()
{
    Vec3f* hFramebuff;
    Vec3f* dFramebuff;

    // Create a few materials
    Material ivory = { {0.4, 0.4, 0.3}, {0.6,  0.3, 0.3}, 50.f };
    Material red_rubber = { {0.3, 0.1, 0.1}, {0.9,  0.1, 0.0}, 10.f };
    Material mirror = { {1.0, 1.0, 1.0}, {0.0, 10.0, 0.8}, 1425.f };
    Material orange = { {1.0, 0.65, 0.0}, {0.8, 2.0, 0.0}, 12.f };

    Material materials[] = { ivory, red_rubber, mirror, orange };

    // Add multiple spheres (for host and device)
    Sphere* hSpheres;
    Sphere* dSpheres;

    // Add lights
    Light* hLights;
    Light* dLights;

    // Read skybox image
    const char* imageName = "envmap2.ppm";
    Image hSkybox, *dSkybox;
    Vec3f* dSkyboxBuff;
    if (!readSkyboxImage(imageName, hSkybox))
    {
        exit(EXIT_FAILURE);
    }

    // Allocate memory for host
    hFramebuff = new Vec3f[WIDTH * HEIGHT];
    hSpheres = new Sphere[NSPHERES];
    hLights = new Light[NLIGHTS];

    // Randomly generate 20 spheres
    double xMax = 20.0, xMin = -20.0;
    double yMax = 10.0, yMin = -10.0;
    double zMax = -10.0, zMin = -30.0;
    double rMin = 0.5, rMax = 6.0;

    //std::random_device dev;
    //std::mt19937 rng(dev());
    std::mt19937 rng;
    rng.seed(303065);
    std::uniform_real_distribution<double> distx(xMin, xMax);
    std::uniform_real_distribution<double> disty(yMin, yMax);
    std::uniform_real_distribution<double> distz(zMin, zMax);
    std::uniform_real_distribution<double> distr(rMin, rMax);
    std::uniform_int_distribution<int> rmat(0, 3);

    for (int i = 0; i < NSPHERES; ++i)
    {
        Sphere s;
        s.center.x = distx(rng);
        s.center.y = disty(rng);
        s.center.z = distz(rng);
        s.radius = distr(rng);
        s.material = materials[rmat(rng)];
        hSpheres[i] = s;
    }

    //hSpheres[0] = { {-3, 0, -16} , 2, orange };
    //hSpheres[1] = { {-1, -1.5, -12}, 2, red_rubber };
    //hSpheres[2] = { {-8, 0.0, -15}, 2, red_rubber };
    //hSpheres[3] = { {1.5, -0.5, -18}, 3, mirror };
    //hSpheres[4] = { {3, 4, -18}, 3, mirror };
    //hSpheres[5] = { {7, 5, -18}, 4, ivory };
    //hSpheres[6] = { {-7, -5, -21}, 5, ivory };

    hLights[0] = { {-20, 20, 20}, 1.5 };
    hLights[1] = { {30, 50, -25}, 1.8 };
    hLights[2] = { {30, 20, 30}, 1.7 };

    // Allocate memory for CUDA
    cudaMalloc(&dSkyboxBuff, sizeof(Vec3f) * hSkybox.width * hSkybox.height);
    cudaMalloc(&dSkybox, sizeof(Image));
    cudaMalloc(&dFramebuff, sizeof(Vec3f) * WIDTH * HEIGHT);
    cudaMalloc(&dSpheres, sizeof(Sphere) * NSPHERES);
    cudaMalloc(&dLights, sizeof(Light) * NLIGHTS);

    // Copy memory from host to device
    cudaMemcpy(dSkyboxBuff, hSkybox.buff, sizeof(Vec3f) * hSkybox.width * hSkybox.height, cudaMemcpyHostToDevice);
    cudaMemcpy(dSkybox, &hSkybox, sizeof(Image), cudaMemcpyHostToDevice);
    cudaMemcpy(dSpheres, hSpheres, sizeof(Sphere) * NSPHERES, cudaMemcpyHostToDevice);
    cudaMemcpy(dLights, hLights, sizeof(Light) * NLIGHTS, cudaMemcpyHostToDevice);

    std::cout << "R/T\t\tTime (s)" << std::endl;
    
    int limit = 8196;

    for (int rpt = 1; rpt <= limit; rpt *= 2)
    {
        //int nThreads = NTHREADS / rpt;
        int nBlocks = std::ceil(HEIGHT * WIDTH / NTHREADS / double(rpt));

        // Start measuring time
        auto start = std::chrono::high_resolution_clock::now();

        // Call kernel
        renderPixelKernel<<<nBlocks, NTHREADS>>>(dFramebuff, dSpheres, dLights, dSkybox, dSkyboxBuff, rpt);

        // Wait for async GPU kernel to end
        cudaDeviceSynchronize();

        // Stop measuring time
        auto stop = std::chrono::high_resolution_clock::now();
        //auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start);
        std::chrono::duration<double> duration = stop - start;
        std::cout << rpt << ";" << duration.count() << std::endl;

        // Copy framebuff from device to host
        cudaMemcpy(hFramebuff, dFramebuff, sizeof(Vec3f) * WIDTH * HEIGHT, cudaMemcpyDeviceToHost);

        // Clear device framebuff
        cudaMemset(dFramebuff, 0, sizeof(Vec3f) * WIDTH * HEIGHT);

        // Check for errors on GPU
        cudaError_t err = cudaGetLastError();
        if (err != 0)
        {
            std::cerr << cudaGetErrorString(err) << std::endl;
            break;
        }

        // Save framebuff to file
        if (rpt == limit)
            saveFrameToFile(hFramebuff);
    }    

    // Deallocate device memory
    cudaFree(dLights);
    cudaFree(dSpheres);
    cudaFree(dFramebuff);
    cudaFree(dSkybox);

    // Deallocate host memory
    delete[] hLights;
    delete[] hSpheres;
    delete[] hFramebuff;
    delete[] hSkybox.buff;

    return 0;
}
